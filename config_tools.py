# author: Katarzyna 'K8' Sosnowska
# e-mail: sosnowska.kk@gmail.com
# 
# date of last update: 2022-06-10
# 
# # About: 
# This file contains helper class for managing reading and writing 
# config files with settings used in Loc Tools UE program.
# 
# .

import os

# config file variables:
MTA_CSV_SOURCE_DIR = "mta_csv_source_dir"
MTA_XLSX_TRANSLATIONS_DIR = "mta_xlsx_translations_dir"
MTA_XLSX_OUTPUT_FILE = "mta_xlsx_output_file"
MTA_SEPARATOR_MARK = "mta_separator_mark"
POU_PO_SOURCE_DIR = "pou_po_source_dir"
POU_PO_OUTPUT_DIR = "pou_po_output_dir"
POU_PO_ID_PREFIX = "pou_po_id_prefix"
POU_XLSX_TRANSLATIONS_DIR = "pou_xlsx_translations_dir"

STR_NONE = "none"
DEFAULT_ID_PREFIX = ','

DEFAULT_CONFIG_FILE_PATH = "./config"

'''
This class encapsulates all settings used in Loc Tools UE program.
'''
class LocToolsConfig:
    # missing translations analyser settings:
    mta_csv_source_dir = STR_NONE
    mta_xlsx_translations_input_dir = STR_NONE
    mta_xlsx_output_file = STR_NONE
    mta_separator_mark = STR_NONE
    # po updater settings:
    pou_po_source_dir = STR_NONE
    pou_po_output_dir = STR_NONE
    pou_po_id_prefix = DEFAULT_ID_PREFIX
    pou_xlsx_translations_dir = STR_NONE

    '''
    Writes down current state of all variables into a file with given path <config_file_path>.
    '''
    def write_config_to_file(self, config_file_path):
        with open(config_file_path, "w") as config_file:
            # generate missing_translations_analyser variables:
            config_file.write("## MTA [Missing Translations Analyser] settings:\n")
            config_file.write("# {0} - a path that leds to a directory with CSV files that contain all texts.\n".format(MTA_CSV_SOURCE_DIR))
            config_file.write("{0}=none\n".format(MTA_CSV_SOURCE_DIR))
            config_file.write("# {0} - a path that leds to a directory with XLSX files with texts translations.\n".format(MTA_XLSX_TRANSLATIONS_DIR))
            config_file.write("{0}=none\n".format(MTA_XLSX_TRANSLATIONS_DIR))
            config_file.write("# {0} - a path that leds to a XLSX output file with all texts that miss translations.\n".format(MTA_XLSX_OUTPUT_FILE))
            config_file.write("{0}=none\n".format(MTA_XLSX_OUTPUT_FILE))
            config_file.write("# {0} - a single character or a phrase used for separating cells with data in CSV files\n# (eg. a comma ',').\n".format(MTA_SEPARATOR_MARK))
            config_file.write("{0}=,\n".format(MTA_SEPARATOR_MARK))
            # generate po_updater variables:
            config_file.write("\n## POU [PO Updater] and [PO Updater ALL] settings:\n")
            config_file.write("# {0} - a path that leds to a directory with PO files that will serve as templates for\n# generating updated output files.\n# Used both in [pou] and [pouall].\n".format(POU_PO_SOURCE_DIR))
            config_file.write("{0}=none\n".format(POU_PO_SOURCE_DIR))
            config_file.write("# {0} - a path that leds to a directory for output PO files that will be created during\n# po_updater job.\n".format(POU_PO_OUTPUT_DIR))
            config_file.write("{0}=none\n".format(POU_PO_OUTPUT_DIR))
            config_file.write("# {0} - a path that leds to a directory with XLSX files with texts translations.\n# Used both in [pou] and [pouall].\n".format(POU_PO_ID_PREFIX))
            config_file.write("{0}=,\n".format(POU_PO_ID_PREFIX))
            config_file.write("# {0} - in PO file exported from Unreal Engine, each text ID has a prefix.\n# We assume that all texts have the same prefix.\n# Used both in [pou] and [pouall].\n".format(POU_XLSX_TRANSLATIONS_DIR))
            config_file.write("# Translations from input XLSX file don't have that prefix, so we need to add it manually.\n")
            config_file.write("{0}=none\n".format(POU_XLSX_TRANSLATIONS_DIR))
        pass

    '''
    Updates variables state with data from a file with given path <config_file_path>.
    '''
    def load_config_data(self, config_file_path):
        if not os.path.exists(config_file_path):
            print("[config_tools] Couldn't load data from file path: {0}!".format(config_file_path))
            return
        with open(config_file_path, "r") as config_file:
            for line in config_file:
                words = line.split('=')
                if len(words) <= 1:
                    continue
                # parsing missing_translations_analyser variables:
                elif words[0] == MTA_CSV_SOURCE_DIR:
                    self.mta_csv_source_dir = words[1].rstrip('\r\n')
                elif words[0] == MTA_XLSX_TRANSLATIONS_DIR:
                    self.mta_xlsx_translations_input_dir = words[1].rstrip('\r\n')
                elif words[0] == MTA_XLSX_OUTPUT_FILE:
                    self.mta_xlsx_output_file = words[1].rstrip('\r\n')
                elif words[0] == MTA_SEPARATOR_MARK:
                    self.mta_separator_mark = words[1].rstrip('\r\n')
                # parsing po_updater variables:
                elif words[0] == POU_PO_SOURCE_DIR:
                    self.pou_po_source_dir = words[1].rstrip('\r\n')
                elif words[0] == POU_PO_OUTPUT_DIR:
                    self.pou_po_output_dir = words[1].rstrip('\r\n')
                elif words[0] == POU_PO_ID_PREFIX:
                    self.pou_po_id_prefix = words[1].rstrip('\r\n')
                elif words[0] == POU_XLSX_TRANSLATIONS_DIR:
                    self.pou_xlsx_translations_dir = words[1].rstrip('\r\n')
        pass

    '''
    This methods returns a tuple with all settings needed for 'mta' tool.
    returns: (mta_csv_source_dir, mta_xlsx_translations_input_dir, mta_xlsx_output_file, mta_separator_mark)
    '''
    def get_missing_translations_analyser_settings(self):
        return self.mta_csv_source_dir, self.mta_xlsx_translations_input_dir, self.mta_xlsx_output_file, self.mta_separator_mark
    
    '''
    This methods returns a tuple with all settings needed for 'pou' tool.
    returns: (pou_po_source_dir, pou_po_output_dir, pou_xlsx_translations_dir, pou_po_id_prefix)
    '''
    def get_po_updater_settings(self):
        return self.pou_po_source_dir, self.pou_po_output_dir, self.pou_xlsx_translations_dir, self.pou_po_id_prefix
    
    '''
    This methods returns a tuple with all settings needed for 'pouall' tool.
    returns: (pou_po_source_dir, pou_xlsx_translations_dir, pou_po_id_prefix)
    '''
    def get_po_updater_all_settings(self):
        return self.pou_po_source_dir, self.pou_xlsx_translations_dir, self.pou_po_id_prefix

'''
Generates new config file if can't find any in specified './config' location.
'''
def generate_new_config():
    config_data = LocToolsConfig()
    config_data.write_config_to_file(DEFAULT_CONFIG_FILE_PATH)
    print("[config_tools] generated config file.")
    pass

'''
Generates new config file if can't find any in specified './config' location.
'''
def generate_config_if_needed():
    if not os.path.exists(DEFAULT_CONFIG_FILE_PATH):
        generate_new_config()
    pass

#===============================================================================================

if __name__== "__main__":
    generate_config_if_needed()
    pass