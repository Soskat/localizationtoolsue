# author: Katarzyna 'K8' Sosnowska
# e-mail: sosnowska.kk@gmail.com
# 
# date of last update: 2022-01-25
# 
# # About: 
# This file contains methods for updating PO files with translations data read from given XLSX files.
# 
# # Base assumptions for this pipeline are as follows:
# - all string tables used in UE project are created from data loaded from CSV files
# - all localization data have been exported from UE built-in Localization system to a bunch of PO files
# - translation company provided a set of XLSX files with texts translations
# - texts IDs in XLSX files are composed with CSV file name and text key used in UE separated by the '+' sign:
#   eg.: 'ST_SomeFile.csv+Some_Text_ID'
# 
# # How PO Updater program works?
# 
# First, it analyses all XLSX files with translations and register all translations in a dedicated dictionary.
# 
# After that, it goes through all input PO files (those are set of texts for all cultures used in UE project) 
# and read their content. For each text ID (msgctxt) that has existing translations in generated translations dictionary 
# its assigned translation text (msgstr) is replaced by updated translation from generated translations dictionary.
# .

import os
import sys
from xlsx_translations_analyser import analyse_xlsx_translations

MSGCTXT = "msgctxt "    # text ID
MSGID = "msgid "        # source text (native)
MSGSTR = "msgstr "      # translation text
PO_FILE_EXT = ".po"

'''
Generates an updated PO file out of input PO file and provided translations dictionary.
'''
def generate_updated_po_file(input_file_path, output_file_path, translations_dict, po_id_prefix):
    debug_translations_count = 0
    with open(input_file_path, "r", encoding="utf8") as input_file:
        with open(output_file_path, "w", encoding="utf8") as output_file:
            file_name = os.path.basename(input_file_path)
            language_code = file_name.replace(PO_FILE_EXT, '')
            text_id = ""
            debug_errors_Count = 0
            debug_texts_count = 0
            # read all lines in input file and write their updated version in output file:
            for line in input_file:
                if line.startswith(MSGCTXT):
                    text_id = line[len(MSGCTXT) + 1 + len(po_id_prefix):-2]
                    debug_texts_count += 1
                    if text_id not in translations_dict:
                        debug_errors_Count += 1
                        pass
                    output_file.write(line)
                elif line.startswith(MSGSTR):
                    if text_id in translations_dict and language_code in translations_dict[text_id]:
                        text_source = translations_dict[text_id][language_code]
                        output_file.write("{0}\"{1}\"\n".format(MSGSTR, text_source))
                        debug_translations_count += 1
                    else:
                        output_file.write(line)
                        debug_errors_Count += 1
                else:
                    output_file.write(line)
                pass
    print("\t> generated \"{0}\" file [{1} texts | {2} updated translations | {3} missing texts]".format(output_file_path, debug_texts_count, debug_translations_count, debug_errors_Count))
    pass

'''
Generates updated PO files for all files from given set of PO source files.
'''
def po_updater_internal(input_dir_path, output_dir_path, translations_input_dir, po_id_prefix):
    if not os.path.exists(input_dir_path):
        print("\t> error: directory {0} doesn't exist!".format(input_dir_path))
        return
    if not os.path.exists(output_dir_path):
        os.mkdir(output_dir_path)
    # load translations data:
    translations_dict = analyse_xlsx_translations(translations_input_dir, 20, False)
    print("> generated {0} texts in translationsDict".format(len(translations_dict)))
    # generate updated PO files:
    for input_file_name in os.listdir(input_dir_path):
        if not input_file_name.endswith(PO_FILE_EXT):
            continue
        input_file_path = os.path.join(input_dir_path, input_file_name)
        output_file_path = os.path.join(output_dir_path, input_file_name)
        generate_updated_po_file(input_file_path, output_file_path, translations_dict, po_id_prefix)
        pass
    pass

'''
Invokes the generation of updated PO files for all files from given set of PO source files.
'''
def po_updater(params):
    po_updater_internal(params[0], params[1], params[2], params[3])
    pass

#===============================================================================================
PROMPT_MARK = '>'

if __name__== "__main__":
    # get PO source files directory path:
    print("[po_updater] enter path to a directory with PO source files:")
    po_source_dir = input(PROMPT_MARK).lstrip(PROMPT_MARK)
    if len(po_source_dir) == 0:
        sys.exit("[po_updater] has ended")
    if not os.path.exists(po_source_dir):
        sys.exit("[po_updater] ERROR: '{0}' directory doesn't exist!".format(po_source_dir))
    # get PO output files directory path:
    print("[po_updater] enter path to a directory with PO output files:")
    po_output_dir = input(PROMPT_MARK).lstrip(PROMPT_MARK)
    if len(po_output_dir) == 0:
        sys.exit("[po_updater] has ended")
    if not os.path.exists(po_output_dir):
        sys.exit("[po_updater] ERROR: '{0}' directory doesn't exist!".format(po_output_dir))
    # get XLSX translations file path:
    print("[po_updater] enter path to a directory with XLSX translations files:")
    xlsx_translations_dir = input(PROMPT_MARK).lstrip(PROMPT_MARK)
    if len(xlsx_translations_dir) == 0:
        sys.exit("[po_updater] has ended")
    if not os.path.exists(xlsx_translations_dir):
        sys.exit("[po_updater] ERROR: '{0}' directory doesn't exist!".format(xlsx_translations_dir))
    # get PO text id prefix:
    print("[po_updater] enter PO texts id prefix:")
    poIdPrefix = input(PROMPT_MARK).lstrip(PROMPT_MARK)
    # run program:
    po_updater_internal(po_source_dir, po_output_dir, xlsx_translations_dir, poIdPrefix)
