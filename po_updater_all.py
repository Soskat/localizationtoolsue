# author: Katarzyna 'K8' Sosnowska
# e-mail: sosnowska.kk@gmail.com
# 
# date of last update: 2022-06-10
# 
# # About: 
# This file contains methods for updating PO files with translations data read from given XLSX files.
# 
# # Base assumptions for this pipeline are as follows:
# - all string tables used in UE project are created from data loaded from CSV files
# - all localization data have been exported from UE built-in Localization system to a bunch of PO files
# - translation company provided a set of XLSX files with texts translations
# - texts IDs in XLSX files are composed with CSV file name and text key used in UE separated by the '+' sign:
#   eg.: 'ST_SomeFile.csv+Some_Text_ID'
# 
# # How PO Updater ALL program works?
# 
# First, it analyses all XLSX files with translations and register all translations in a dedicated dictionary.
# 
# After that, it goes through all input PO files (those are set of texts for all cultures used in UE project) 
# found inside subdirectories of input directory and read their content. For each text ID (msgctxt) that has 
# existing translations in generated translations dictionary its assigned translation text (msgstr) 
# is replaced by updated translation from generated translations dictionary.
# .

import os
import sys
from xlsx_translations_analyser import analyse_xlsx_translations

MSGCTXT = "msgctxt "    # text ID
MSGID = "msgid "        # source text (native)
MSGSTR = "msgstr "      # translation text
PO_FILE_EXT = ".po"

'''
Overrides PO file texts with provided translations dictionary.
'''
def update_po_file(input_file_path, culture_code, translations_dict, po_id_prefix):
    updated_lines = []
    debug_translations_count = 0
    debug_errors_count = 0
    debug_texts_count = 0
    # go line by line and update it with translation when applicable:
    with open(input_file_path, "r", encoding="utf8") as input_file:
        text_id = ""
        for line in input_file:
            if line.startswith(MSGCTXT):
                text_id = line[len(MSGCTXT) + 1 + len(po_id_prefix):-2]
                debug_texts_count += 1
                if text_id not in translations_dict:
                    debug_errors_count += 1
                    pass
                updated_lines.append(line)
            elif line.startswith(MSGSTR):
                if text_id in translations_dict and culture_code in translations_dict[text_id]:
                    text_source = translations_dict[text_id][culture_code]
                    updated_lines.append("{0}\"{1}\"\n".format(MSGSTR, text_source))
                    debug_translations_count += 1
                else:
                    updated_lines.append(line)
                    debug_errors_count += 1
            else:
                updated_lines.append(line)
        pass
    # save updated file:
    with open(input_file_path, "w", encoding="utf8") as result_file:
        result_file.writelines(updated_lines)
    # log summary:
    print("\t> updated \"{0}\" file [{1} texts | {2} updated translations | {3} missing texts]".format(input_file_path, debug_texts_count, debug_translations_count, debug_errors_count))
    pass

'''
Searches all subdirectories inside input_dir_path for PO files and updates them with translations.
'''
def update_all_po_files_in_directory(input_dir_path, translations_input_dir, po_id_prefix):
    if not os.path.exists(input_dir_path):
        print("\t> error: directory {0} doesn't exist!".format(input_dir_path))
        return
    # load translations data:
    translations_dict = analyse_xlsx_translations(translations_input_dir, 20, False)
    print("> generated {0} texts in translationsDict".format(len(translations_dict)))
    # update all PO files found in input dir:
    for loc_dir in os.scandir(input_dir_path):
        if not loc_dir.is_dir():
            continue
        print("> checking \'{0}\' directory".format(loc_dir.name))
        for file in os.scandir(loc_dir.path):
            if not file.is_file() or not file.name.endswith(PO_FILE_EXT):
                continue
            update_po_file(file.path, loc_dir.name, translations_dict, po_id_prefix)
    pass

'''
Invokes the generation of updated PO files found inside given localization data directory.
'''
def po_updater_all(params):
    update_all_po_files_in_directory(params[0], params[1], params[2])
    pass

#===============================================================================================
PROMPT_MARK = '>'

if __name__== "__main__":
    # get localization directory path:
    print("[po_updater_all] enter path to a directory that contains localization data for UE project:")
    input_dir_path = input(PROMPT_MARK).lstrip(PROMPT_MARK)
    if len(input_dir_path) == 0:
        sys.exit("[po_updater_all] has ended")
    if not os.path.exists(input_dir_path):
        sys.exit("[po_updater_all] ERROR: '{0}' directory doesn't exist!".format(input_dir_path))
    # get path to a directory with XLSX translations files:
    print("[po_updater_all] enter path to a directory with XLSX translations files:")
    xlsx_translations_dir = input(PROMPT_MARK).lstrip(PROMPT_MARK)
    if len(xlsx_translations_dir) == 0:
        sys.exit("[po_updater_all] has ended")
    if not os.path.exists(xlsx_translations_dir):
        sys.exit("[po_updater_all] ERROR: '{0}' directory doesn't exist!".format(xlsx_translations_dir))
    # get PO text id prefix:
    print("[po_updater_all] enter PO texts id prefix:")
    po_id_prefix = input(PROMPT_MARK).lstrip(PROMPT_MARK)
    # run program:
    update_all_po_files_in_directory(input_dir_path, xlsx_translations_dir, po_id_prefix)
