# author: Katarzyna 'K8' Sosnowska
# e-mail: sosnowska.kk@gmail.com
# 
# date of last update: 2022-06-10
# 
# # About: 
# This program is an entry point for using tools dedicated to a particular pipeline used 
# with Unreal Engine built-in Localization system.
# 
# Here, you have access to two following tools:
# 
# Missing Translations Analyser:
#  > command: mta
#  > description: Searches texts with missing translations in input CSV files.
#                 All missing translations are written into generated XLSX file.
#                 Each text ID is combined from file name and real text ID.
#                 All input arguments are read from config file (`mta` prefix).
# 
# PO Updater:
#  > command: pou
#  > description: Updates PO files with translations loaded from selected XLSX files.
#                 All input arguments are read from config file (`pou` prefix).
# 
# PO Updater ALL:
#  > command: pouall
#  > description: Updates all PO files found in input directory subdirectories 
#                 with translations loaded from selected XLSX files.
#                 All input arguments are read from config file (`pou` prefix).
# 
# .

from config_tools import LocToolsConfig
from config_tools import generate_new_config, generate_config_if_needed
from po_updater import po_updater
from po_updater_all import po_updater_all
from xlsx_translations_analyser import generate_list_of_missing_translations

# program commands and arguments:
PROMPT_MARK = "> "
HELP_SHORT = "-h"
HELP_FULL = "help"
QUIT_OPTION = "quit"
MAKE_CONFIG = "mkconfig"
MISSING_TRANSLATIONS_ANALYSER_TOOL = "mta"
PO_UPDATER_TOOL = "pou"
PO_UPDATER_ALL_TOOL = "pouall"

# program messages:
WELCOME_MESSAGE = '''
[Localization Tools UE] is ON
Start using it or pass {} or {} for more info.
'''.format(HELP_SHORT, HELP_FULL)
QUIT_MESSAGE = '''[Localization Tools UE] program ended.'''
UNRECOGNISED_COMMAND_MESSAGE = "Unrecognized command. Pass {} or {} to see the program manual.".format(HELP_SHORT, HELP_FULL)
MANUAL_MESSAGE = '''Localization Tools UE - available options:
-h, help    Prints this help

quit        Quits the Localization Tools UE program

mkconfig    Generates a config file for mta and pou tools.
            (note that config file is being automatically generated 
            at the start of [Localization Tools UE] program.

mta         Searches texts with missing translations in input CSV files.
            All missing translations are written into generated XLSX file.
            Each text ID is combined from file name and real text ID.
            All input arguments are read from config file (mta prefix).

pou         Updates PO files with translations loaded from selected XLSX files.
            All input arguments are read from config file (pou prefix).

pouall      Updates all PO files found in input dir with translations loaded 
            from selected XLSX files.
            All input arguments are read from config file (pou prefix).\n\n'''

CONFIG_FILE_PATH = "./config"

def use_missing_translations_analyser():
    # run missing_translations_analyser tool:
    config_file = LocToolsConfig()
    config_file.load_config_data(CONFIG_FILE_PATH)
    generate_list_of_missing_translations(config_file.get_missing_translations_analyser_settings())
    pass

def use_po_updater_tool():
    # run po updater tool:
    config_file = LocToolsConfig()
    config_file.load_config_data(CONFIG_FILE_PATH)
    po_updater(config_file.get_po_updater_settings())
    pass

def use_po_updater_all_tool():
    # run po updater all tool:
    config_file = LocToolsConfig()
    config_file.load_config_data(CONFIG_FILE_PATH)
    po_updater_all(config_file.get_po_updater_all_settings())
    pass

#===============================================================================================

if __name__== "__main__":
    generate_config_if_needed()
    print(WELCOME_MESSAGE)
    while True:
        args = input(PROMPT_MARK).split(' ')
        if len(args) == 0:
            continue
        selectedTool = args[0]
        # print help message:
        if selectedTool in [HELP_SHORT, HELP_FULL]:
            print(MANUAL_MESSAGE)
            continue
        # quit the program:
        elif selectedTool == QUIT_OPTION:
            break
        # selectedTool is 'mkconfig':
        elif selectedTool == MAKE_CONFIG:
            generate_new_config()
        # selectedTool is 'mta':
        elif selectedTool == MISSING_TRANSLATIONS_ANALYSER_TOOL:
            use_missing_translations_analyser()
        # selectedTool is 'pou':
        elif selectedTool == PO_UPDATER_TOOL:
            use_po_updater_tool()
        # selectedTool is 'pouall':
        elif selectedTool == PO_UPDATER_ALL_TOOL:
            use_po_updater_all_tool()
        # unrecognised command message:
        else:
            print(UNRECOGNISED_COMMAND_MESSAGE)
            continue
    print(QUIT_MESSAGE)