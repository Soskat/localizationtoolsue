# author: Katarzyna 'K8' Sosnowska
# e-mail: sosnowska.kk@gmail.com
# 
# date of last update: 2022-06-10
# 
# # About: 
# This file contains methods for analysing translations found in XLSX files and generating a list of texts with missing translations.
# 
# # Base assumptions for this pipeline are as follows:
# - all string tables used in UE project are created from data loaded from CSV files
# - all localization data have been exported from UE built-in Localization system to a bunch of PO files
# - translation company provided a set of XLSX files with texts translations
# - texts IDs in XLSX files are composed with CSV file name and text key used in UE separated by the '+' sign:
#   eg.: 'ST_SomeFile.csv+Some_Text_ID'
# 
# # How MTA program works?
# 
# First, it analyses all XLSX files with translations and register all translations in a dedicated dictionary.
# 
# After that, it goes through all input CSV files (those files are used to create string tables used in UE project) 
# and read their content. Each text ID from CSV file that is not in generated translations dictionary is written down 
# into output XLSX file. As a result, the output XLSX file will contain all texts that are missing their translations.
# .

import os
import sys
from openpyxl import load_workbook  # for working with xlsx files
from openpyxl import Workbook

XLSX_FILE_EXT = ".xlsx"
CSV_EXT = '.csv'

STR_TEXT_ID = "id"
STR_COMMENTS = "comments"
'''
Analyses texts translations from all input file from a given directory.
Param: inputDirPath - a path to a directory with input XLSX files that contain translations data.
Returns: generated dictionary with translations.
'''
def analyse_xlsx_translations(_input_dir_path, _max_column_number, _debug_on):
    if not os.path.exists(_input_dir_path):
        print("[xlsx_translations_analyser] ERROR: \"{0}\" directory doesn't exist!".format(_input_dir_path))
        return {}
    # analyse translations from all XLSX files:
    if _debug_on: print("[xlsx_translations_analyser] : analysing XLSX files in \"{0}\" directory".format(_input_dir_path))
    debug_analysed_texts_num = 0
    translations_dic = {}
    for input_file_name in os.listdir(_input_dir_path):
        if not input_file_name.endswith(".xlsx"):
            continue
        input_file_path = os.path.join(_input_dir_path, input_file_name)
        if _debug_on: print("  > analysing \"{0}\" file...".format(input_file_name))
        wb = load_workbook(input_file_path)
        ws = wb.active
        # detect columns content:
        column_headers_map = {}
        ids_column = "A"
        for col in ws.iter_cols(min_row=1, min_col=1, max_col=_max_column_number):
            cell = col[0]
            if cell.value is None:
                continue
            cell_str = str(cell.value).lower()
            if cell_str == STR_TEXT_ID:
                ids_column = cell.column_letter
                continue
            elif cell_str == STR_COMMENTS:
                continue
            column_headers_map[cell.column_letter] = cell.value
            if _debug_on: print("\t> found culture: \"{0}\"-\"{1}\"".format(cell.column_letter, cell.value))
            pass
        # analyse all found translation data:
        num_of_elements = len(ws[ids_column])
        for row_index in range(2, num_of_elements):
            text_id_raw = ws['{0}{1}'.format(ids_column, row_index)].value
            if text_id_raw is None:
                continue
            text_id_elements = text_id_raw.split('.csv+')
            if len(text_id_elements) < 2:
                continue
            text_id = text_id_elements[1].rstrip('\r\n')
            if text_id not in translations_dic:
                translations_dic[text_id] = {}
            for letter, culture_code in column_headers_map.items():
                cell = ws['{0}{1}'.format(letter, row_index)]
                if cell.value is None:
                    continue
                translations_dic[text_id][culture_code] = cell.value
            debug_analysed_texts_num += 1
            pass
    if _debug_on: print("[xlsx_translations_analyser] : analysed {0} texts".format(debug_analysed_texts_num))
    return translations_dic

'''
Helper method that creates a new workbook and sets the name of initial sheet to given one.
'''
def create_new_workbook(sheet_name):
    wb = Workbook()
    ws = wb.active
    ws.title = sheet_name
    ws['A1'] = "ID"
    ws['B1'] = "Context"
    ws['C1'] = "Source Text"
    return wb

'''
Generates a list of texts that can't be found in provided XLSX files with translations.
All texts that will be on that list are missing translations.
'''
def generate_list_of_missing_translations_internal(csv_source_dir, xlsx_translations_dir, xlsx_output_file, separator):
    if not os.path.exists(csv_source_dir):
        print("[analyse_missing_translations] ERROR: {0} directory doesn't exist!".format(csv_source_dir))
        return
    if not os.path.exists(xlsx_translations_dir):
        print("[analyse_missing_translations] ERROR: {0} directory doesn't exist!".format(xlsx_translations_dir))
        return
    # remove previous XLSX output file:
    if os.path.exists(xlsx_output_file):
        os.remove(xlsx_output_file)
    max_column_number = 20
    debug_on = False
    translations_dict = analyse_xlsx_translations(xlsx_translations_dir, max_column_number, debug_on)
    wb = create_new_workbook('Missing translations')
    ws = wb.active
    # write column headlines:
    ws['A1'] = "ID"
    ws['B1'] = "Context"
    ws['C1'] = "Source text"
    # some stats:
    debug_total_text_count = 0
    debug_total_missing_text_count = 0
    # go through all source CSV files and check if they exist in translationsDict;
    # if not, then log such texts in output XLSX file:
    row_index = 2
    for file_name in os.listdir(csv_source_dir):
        debug_texts_count = 0
        debug_missing_texts_count = 0
        if file_name.endswith(CSV_EXT):
            file_full_name = os.path.join(csv_source_dir, file_name)
            input_file = open(file_full_name, "r", encoding='utf8')
            for line in input_file.readlines():
                words = line.split(separator)
                if len(words) < 2:
                    continue
                elif words[0] in ["Key", "\"Key"] or len(words[0]) < 1:
                    continue
                text_id = words[0][1:]
                if text_id not in translations_dict:
                    ws['A{0}'.format(row_index)].value = "{0}+{1}".format(file_name, text_id)
                    if len(words) > 2:
                        ws['C{0}'.format(row_index)].value = words[1]
                        ws['B{0}'.format(row_index)].value = words[2].rstrip('\"\n')
                    else:
                        ws['C{0}'.format(row_index)].value = words[1].rstrip('\",\n')
                    row_index += 1
                    debug_missing_texts_count += 1
                debug_texts_count += 1
            input_file.close()
            print("{0} [{1} texts checked | {2} missing texts]".format(file_name, debug_texts_count, debug_missing_texts_count))
            debug_total_text_count += debug_texts_count
            debug_total_missing_text_count += debug_missing_texts_count
        else:
            continue
    wb.save(xlsx_output_file)
    print("\nSUMMARY: {0} total texts checked | {1} total missing texts\n".format(debug_total_text_count, debug_total_missing_text_count))
    pass


'''
Invokes generation of a list of texts with missing translations.
'''
def generate_list_of_missing_translations(params):
    generate_list_of_missing_translations_internal(params[0], params[1], params[2], params[3])
    pass

#===============================================================================================
PROMPT_MARK = '>'

if __name__== "__main__":
    print("[missing_transations_analyser] enter path to a directory with input CSV files:")
    csv_source_dir = ""
    csv_source_dir = input(PROMPT_MARK).lstrip(PROMPT_MARK)
    if len(csv_source_dir) == 0:
        sys.exit("[missing_transations_analyser] has ended")
    if not os.path.exists(csv_source_dir):
        sys.exit("[missing_transations_analyser] ERROR: {0} directory doesn't exist!".format(csv_source_dir))
    # get XLSX input file path:
    print("[missing_transations_analyser] enter path to a directory with input XLSX files with translations:")
    xlsx_translations_dir = ""
    xlsx_translations_dir = input(PROMPT_MARK).lstrip(PROMPT_MARK)
    if len(xlsx_translations_dir) == 0:
        sys.exit("[missing_transations_analyser] has ended")
    if not os.path.exists(xlsx_translations_dir):
        sys.exit("[missing_transations_analyser] ERROR: {0} directory doesn't exist!".format(xlsx_translations_dir))
    # get XLSX output file path:
    print("[missing_transations_analyser] enter path of XLSX output file:")
    xlsx_output_file = ""
    xlsx_output_file = input(PROMPT_MARK).lstrip(PROMPT_MARK)
    if len(xlsx_output_file) == 0:
        sys.exit("[missing_transations_analyser] has ended")
    # get separator character:
    print("[missing_transations_analyser] enter separator character (eg. ','):")
    separator = ""
    separator = input(PROMPT_MARK).lstrip(PROMPT_MARK)
    # run program:
    generate_list_of_missing_translations_internal(csv_source_dir, xlsx_translations_dir, xlsx_output_file, separator)
    pass
