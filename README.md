# LocalizationToolsUE

This project provides several tools designed to be used with Unreal Engine built-in localization system.
You can find here a bunch of Python scripts for parsing CSV, PO and XLSX files.

### Wiki
Learn more about each one of tools and the purpose of their existence [on project wiki](https://gitlab.com/Soskat/localizationtoolsue/-/wikis/home).

### Executables
Inside [Executables folder](https://gitlab.com/Soskat/localizationtoolsue/-/tree/main/Executables) there is one executable build for _`loc_tools_ue.py`_ file.
